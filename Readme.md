# Muh blog

A website built on [Hugo](https://gohugo.io/), the static site generator.

## Adding content

You can manually create content files (for example as
content/<CATEGORY>/<FILE>.<FORMAT>) and provide metadata in them, however you
can use the new command to do few things for you (like add title and date):

	hugo new posts/my-first-post.md

## Running Hugo

Start the Hugo server (with drafts enabled):

	hugo server -D

The site will be available at http://localhost:1313/.

## Build static pages

It's simple, just call:

	hugo

Output will be in ./public/ directory by default (-d/--destination flag to
change it, or set publishdir in the config file).

Drafts do not get deployed; once you finish a post, update the header of the
post to say `draft: false`. More info [here](https://gohugo.io/getting-started/usage/#draft-future-and-expired-content).

## Deploying

The website is hosted on GitLab Pages; the [repository](https://gitlab.com/lyle_/lyle_.gitlab.io)
is automatically published by GitLab at [https://lyle_.gitlab.io/](https://lyle_.gitlab.io/),
and I use a CNAME and TXT record with my DNS provider to redirect.

See https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/#for-subdomains
