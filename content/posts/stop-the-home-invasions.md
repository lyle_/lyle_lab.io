---
title: "Stop the Home Invasions"
date: 2019-09-05T09:43:36-05:00
draft: true
---

------------------------
(Uncategorized content)
* XDG Crusade
	- IdeaVim doesn't respect XDG
		https://youtrack.jetbrains.com/issue/VIM-664
		https://github.com/JetBrains/ideavim
	- Docker doesn't respect XDG either:
		- https://github.com/docker/cli/issues/1086
		- https://github.com/docker/cli/blob/master/docs/reference/commandline/cli.md#configuration-files
	- Gradle is another XDG offender
		https://github.com/gradle/gradle/issues/8262
		https://github.com/gradle/gradle/search?q=%24HOME%2F.gradle%2F&unscoped_q=%24HOME%2F.gradle%2F
---------------------

# Stop the $HOME invasions

https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html

## Benefits

TODO: backing up and syncing configuration
TODO: deleting sensitive information (VPNs in hostile countries)
As for tangible benefits, here are a few:
* Users can back up or version control all of their custom configurations from a single directory tree. Sync `$XDG_CONFIG_HOME` and you're done. Contrast that with having to maintain a list of snowflake directories for each application which doesn't follow the spec, likely with regexes and exceptions in cases where an application doesn't use distinct directories for configs vs. data vs. cache.
* Users can rely on the fact that data in `$XDG_CACHE_HOME` can safely be deleted to free up space, or in some privacy-sensitive applications (VPN users in repressive countries) they may want to explicitly do so after every session.

## Arguments against respecting the XDG spec

### ...but it's bad to have files spread in multiple locations!
TODO: that's exactly what you're doing when you arbitrarily decide to create
a new directory hierarchy for your application in a user's `$HOME`.

### ...but users need to know about the XDG spec and set environment variables!
They don't.

## Get involved

Submitting patches to implement the XDG specification is a great way to make a
first contribution to a project or to try your hand at participating in open
source yourself. The logic to locate configuration files tends not to be overly
complex, and many libraries exist to follow the specification for you. It's also
a great first step when working with a programming language you're unfamiliar
with since the problem is tightly constrained and you can get a quick win.

Please vote, comment, or submit PRs on these open issues if you'd like to see
these projects respect your preferences:

* Arduino: https://github.com/arduino/Arduino/issues/8659, https://github.com/arduino/Arduino/issues/3915, https://github.com/arduino/Arduino/issues/7651
* Eddie: https://github.com/AirVPN/Eddie/issues/96
* aragon-cli: https://github.com/aragon/aragon-cli/issues/355
* aspell: https://github.com/GNUAspell/aspell/issues/560
* cloudflared: https://github.com/cloudflare/cloudflared/issues/119
* gauge: https://github.com/getgauge/gauge/issues/1458
* gemrb: https://github.com/gemrb/gemrb/issues/103
* guiscrcpy: https://github.com/srevinsaju/guiscrcpy/issues/7
* gvpngate: https://github.com/Gwiz65/gvpngate/issues/2
* remacs: https://github.com/remacs/remacs/issues/1540
* terraform: https://github.com/hashicorp/terraform/issues/15389

## Patches I've submitted

* aerc: TODO (merged)
* ahubu: https://github.com/ahungry/ahubu/pull/2 (merged)
* slack-term: https://github.com/erroneousboat/slack-term/pull/204 (closed, https://github.com/erroneousboat/slack-term/issues/170)
* vimpc: https://github.com/boysetsfrog/vimpc/pull/83 (merged)

## Libraries implementing the spec

Go: https://github.com/OpenPeeDeeP/xdg
Java: https://github.com/omajid/xdg-java
Python: https://pypi.org/project/xdg

## Other resources

https://wiki.archlinux.org/index.php/XDG_Base_Directory

