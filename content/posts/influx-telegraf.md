---
title: "Influx Telegraf"
date: 2019-10-27T01:20:36-05:00
draft: true
---

Lessons learned from trying to get Telegraf to parse IoT data from MQTT and parse
it into easily consumable InfluxDB data:

	- it's hard to debug plugins you're unfamiliar with when you don't know what
	they're doing https://github.com/influxdata/telegraf/issues/6584
	- The entire TICK stack is very Go-oriented (a good thing), but knowing when
	to write Go regex with what quote characters and how that interacts with TOML
	is a pain
	- You need to understand how TOML works to express the Telegraf configuration
	you have in mind... [[this.that]] is different than [this.that].
	Indentation doesn't seem to matter, and order of plugin definition doesn't
	either, so plugins should have `order = n` declarations if the order of
	processing matters.
	- Is the order zero-based? 1-based? No way to know from the docs.

