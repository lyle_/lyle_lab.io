---
title: "Tview Go Tui"
date: 2019-10-07T17:48:52-05:00
draft: true
---

TODO: Writeup of my experiences using tview: https://github.com/rivo/tview


Lacking: No high-level way to reference widgets; if I have a handler set
in a widget in a layout right next to another one, tview does not provide me
any way to change focus to another widget without me having to keep track of
other widgets in my own code.


Bummer: I spent way too much time working on organizing my TUI and inventing
my own schemes of tracking other widgets and transitioning focus between them.

